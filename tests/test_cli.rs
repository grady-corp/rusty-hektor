use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

use anyhow::Result;

#[test]
fn needs_input_file() -> Result<()> {
    let mut cmd = Command::cargo_bin("rusty-hektor")?;
    cmd.assert().failure().stderr(predicate::str::contains(
        "error: The following required arguments were not provided:",
    ));

    Ok(())
}

// TODO there should be more tests here....
