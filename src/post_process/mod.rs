use std::collections::{HashMap, HashSet};
use std::mem::replace;

use anyhow::Result;
use rand::{seq::SliceRandom, thread_rng};
use serde::{Deserialize, Serialize};

use crate::output_types::HektorOutput;
use indicatif::{ProgressBar, ProgressIterator};

pub mod test_runner;
pub use test_runner::*;

const WORD_LIST: &str = include_str!("../wordlist/eff_large_wordlist.txt");

pub struct PostProcessContext {
    pub anonymize: bool,
    pub tests: Vec<Box<dyn Test>>,
}

#[derive(Serialize, Deserialize)]
pub struct StudentAnonMapping {
    #[serde(rename = "matrikelNo")]
    pub identifier: String,
    #[serde(rename = "name")]
    pub fullname: String,
    pub email: String,
}

pub type AnonIdentifier = String;
pub type StudentAnonMap = HashMap<AnonIdentifier, StudentAnonMapping>;

pub fn post_process(
    mut hektor_output: HektorOutput,
    context: &PostProcessContext,
) -> Result<(HektorOutput, Option<StudentAnonMap>)> {
    // Run every test for every submission of every student
    let pb = ProgressBar::new(hektor_output.data.students.len() as u64);
    pb.println("Ruuning tests on student submissions");
    for student in hektor_output.data.students.iter_mut().progress_with(pb) {
        for submission in student.submissions.iter_mut() {
            submission.tests = context
                .tests
                .iter()
                .map(|test| test.run(&submission))
                .collect::<Result<Vec<_>>>()?;
        }
    }

    let map = if context.anonymize {
        Some(anonymize(&mut hektor_output))
    } else {
        None
    };

    Ok((hektor_output, map))
}

fn anonymize(hektor_output: &mut HektorOutput) -> StudentAnonMap {
    let num_students = hektor_output.data.students.len();
    let anon_idents = generate_unique_anon_identifiers(num_students);

    let mut student_map = HashMap::with_capacity(num_students);
    for (student, random_ident) in hektor_output
        .data
        .students
        .iter_mut()
        .zip(anon_idents.into_iter())
    {
        let identifier = replace(
            &mut student.identifier,
            random_ident.delimeted_identifier.clone(),
        );
        let fullname = replace(&mut student.fullname, random_ident.capitalised_name);
        let email = replace(&mut student.email, "".into());
        student.username = random_ident.delimeted_identifier.clone();
        student_map.insert(
            random_ident.delimeted_identifier,
            StudentAnonMapping {
                identifier,
                fullname,
                email,
            },
        );
    }
    student_map
}

struct RandomIdentifier {
    capitalised_name: String,
    delimeted_identifier: String,
}

fn generate_unique_anon_identifiers(num_identifiers: usize) -> Vec<RandomIdentifier> {
    let mut rng = thread_rng();
    let mut used_identifiers = HashSet::new();
    let word_list = generate_wordlist();
    let mut identifiers = Vec::new();
    while identifiers.len() != num_identifiers {
        let first = word_list.choose(&mut rng).expect("Word list is empty");
        let second = word_list.choose(&mut rng).expect("Word list is empty");
        let delimeted_identifier = format!("{}-{}", first, second);
        if first == second || used_identifiers.contains(&delimeted_identifier) {
            continue;
        }
        used_identifiers.insert(delimeted_identifier.clone());
        let capitalised_name = format!("{} {}", capitalize_str(first), capitalize_str(second));
        identifiers.push(RandomIdentifier {
            capitalised_name,
            delimeted_identifier,
        })
    }

    identifiers
}

fn generate_wordlist() -> Vec<&'static str> {
    WORD_LIST.split_whitespace().collect()
}

// from https://stackoverflow.com/a/38406885
fn capitalize_str(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}
