use anyhow::Result;

use crate::output_types::submission::Submission;
use crate::output_types::test_output::TestOutput;

pub trait Test {
    fn run(&self, submission: &Submission) -> Result<TestOutput>;
}

pub mod empty_test {
    use std::fmt::{self, Display, Formatter};

    use super::Test;

    use super::*;

    enum Labels {
        Empty,
        NotEmpty,
    }

    impl Display for Labels {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            match self {
                Labels::Empty => write!(f, "EMPTY"),
                Labels::NotEmpty => write!(f, "NOT_EMPTY"),
            }
        }
    }

    pub struct EmptyTest {}

    impl Test for EmptyTest {
        fn run(&self, submission: &Submission) -> Result<TestOutput> {
            let label = match submission.code.trim().len() {
                0 => Labels::Empty,
                _ => Labels::NotEmpty,
            };
            Ok(TestOutput {
                name: "EmptyTest".to_string(),
                annotation: "".to_string(),
                label: label.to_string(),
            })
        }
    }
}

pub mod compile_test {
    use super::Test;
    use crate::output_types::submission::Submission;
    use crate::output_types::test_output::TestOutput;
    use anyhow::{Context, Result};
    use std::ffi::OsString;
    use std::fmt::{self, Display, Formatter};
    use std::io::Write;
    use std::path::PathBuf;
    use std::process::{Command, Stdio};
    use tempfile::tempdir;

    enum Labels {
        CompilationSuccessful,
        CompilationFailed,
    }

    impl Display for Labels {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            match self {
                Labels::CompilationFailed => write!(f, "COMPILATION_FAILED"),
                Labels::CompilationSuccessful => write!(f, "COMPILATION_SUCCESSFUL"),
            }
        }
    }

    pub struct CompileTest {
        /// Contains either the program name of the compile found in the PATH
        /// or an explicit path to the binary
        compiler: OsString,
        compile_args: Vec<OsString>,
    }

    impl CompileTest {
        pub fn new(header_dir: PathBuf) -> Self {
            let args = vec![
                "-Wall".into(),
                "-Wextra".into(),
                "-c".into(),
                "-x".into(),
                "c".into(),
                "-std=c11".into(),
                "-I".into(),
                header_dir.as_os_str().into(),
                "-o".into(),
                "code.o".into(),
                "-".into(), // read program from stdin
            ];

            Self {
                compiler: "gcc".into(),
                compile_args: args,
            }
        }
    }

    impl Test for CompileTest {
        fn run(&self, submission: &Submission) -> Result<TestOutput> {
            let tmp_dir = tempdir().context("Error creating temporary dir")?;
            let output = {
                let mut child = Command::new(&self.compiler)
                    .args(&self.compile_args)
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .stderr(Stdio::piped())
                    // DO NOT MOVE THE tmp_dir in... this cost me a lot of
                    // time. Moving the TempDir will lead to an early drop
                    // and result in an OsCode Error 2 - No such file or dir
                    .current_dir(&tmp_dir)
                    .spawn()
                    .context("Unable to spawn compile command")?;
                child
                    .stdin
                    .as_mut()
                    .unwrap()
                    .write_all(submission.code.as_bytes())
                    .context("Unable to write to compiler stdin")?;
                child.wait_with_output().context("")?
            };

            let label = if output.status.success() {
                Labels::CompilationSuccessful
            } else {
                Labels::CompilationFailed
            };

            Ok(TestOutput {
                name: "CompileTest".into(),
                annotation: String::from_utf8_lossy(&output.stderr).to_string(),
                label: label.to_string(),
            })
        }
    }
}
