use serde::Serialize;

use crate::output_types::module::Module;
use crate::output_types::student::Student;
use crate::output_types::submission_type::SubmissionType;

#[derive(Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Exam {
    pub module: Module,
    pub exam_start_timestamp: u32,
    pub submission_types: Vec<SubmissionType>,
    pub students: Vec<Student>,
}
