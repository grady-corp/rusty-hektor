use derive_builder::Builder;
use serde::Serialize;

use crate::output_types::submission::Submission;

#[derive(Builder, Debug, Serialize, Default, PartialEq, Eq, PartialOrd, Ord, Clone)]
#[builder(derive(Debug))]
pub struct Student {
    pub fullname: String,
    pub identifier: String,
    pub username: String,
    #[builder(default = "String::new()")]
    pub email: String,
    #[builder(default = "Vec::new()")]
    pub exercise_groups: Vec<String>,
    pub submissions: Vec<Submission>,
}
