use serde::Serialize;

use crate::output_types::test_output::TestOutput;

#[derive(Debug, Eq, PartialEq, Serialize, Default, Clone, Hash, PartialOrd, Ord)]
pub struct Submission {
    pub code: String,
    /// This field is populated if the displayed source code differs from the
    /// source. We use this for the correction of `.ipynb` notebooks which we transform
    /// into a python script to display but want to keep the original notebook as json
    pub source_code: Option<String>,
    #[serde(rename = "type")]
    pub type_name: String,
    pub tests: Vec<TestOutput>,
}
