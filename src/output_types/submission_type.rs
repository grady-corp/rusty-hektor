use std::fmt::{self, Display};
use std::str::FromStr;

// use serde::export::Formatter;
use serde::Serialize;

use anyhow::{anyhow, Error};

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, PartialOrd, Ord)]
pub struct SubmissionType {
    pub name: String,
    pub full_score: u32,
    pub description: String,
    pub solution: String,
    pub programming_language: ProgrammingLang,
}

#[derive(Debug, Clone, Copy, Serialize, Eq, PartialEq, Hash, PartialOrd, Ord)]
#[serde(rename_all = "lowercase")]
pub enum ProgrammingLang {
    C,
    Java,
    Mipsasm,
    Haskell,
    Python,
    Plaintext,
    Markdown,
}

impl ProgrammingLang {
    pub const fn list_available() -> &'static str {
        "c, java, mipsasm, haskell, plaintext, markdown, python"
    }
}

impl Default for ProgrammingLang {
    fn default() -> Self {
        Self::Plaintext
    }
}

impl Display for ProgrammingLang {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> fmt::Result {
        let display = match self {
            ProgrammingLang::C => "c",
            ProgrammingLang::Java => "java",
            ProgrammingLang::Mipsasm => "mipsasm",
            ProgrammingLang::Haskell => "haskell",
            ProgrammingLang::Plaintext => "plaintext",
            ProgrammingLang::Markdown => "markdown",
            ProgrammingLang::Python => "python",
        };
        write!(f, "{}", display)
    }
}

impl FromStr for ProgrammingLang {
    // TODO rework the error type here
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "c" => ProgrammingLang::C,
            "java" => ProgrammingLang::Java,
            "mipsasm" => ProgrammingLang::Mipsasm,
            "haskell" => ProgrammingLang::Haskell,
            "plaintext" => ProgrammingLang::Plaintext,
            "markdown" => ProgrammingLang::Markdown,
            "python" => ProgrammingLang::Python,
            unknown => return Err(anyhow!(
                "Unknown programming language '{}'. Available: {}",
                unknown,
                ProgrammingLang::list_available()
            )),
        })
    }
}
