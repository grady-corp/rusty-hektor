use serde::Serialize;

use crate::output_types::exam::Exam;
use crate::output_types::meta::MetaInformation;

pub mod exam;
pub mod meta;
pub mod module;
pub mod student;
pub mod submission;
pub mod submission_type;
pub mod test_output;

#[derive(Serialize, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct HektorOutput {
    pub meta: MetaInformation,
    pub data: Exam,
}
