use serde::Serialize;

#[derive(Serialize, Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct MetaInformation {
    pub version: String,
}

impl MetaInformation {
    pub fn new() -> Self {
        Self {
            version: env!("CARGO_PKG_VERSION").to_string(),
        }
    }
}
