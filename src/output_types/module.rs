use serde::Serialize;

use crate::io::{input, yes_or_no};

#[derive(Debug, Default, Serialize, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Module {
    module_reference: String,
    total_score: u32,
    pass_score: u32,
    pass_only: bool,
}

impl Module {
    pub fn from_input() -> Module {
        println!("As additional information, some data about the module corresponding to this data is asked.");

        Module {
            module_reference: input("Module reference number: "),
            total_score: input("Total score: "),
            pass_score: input("Pass score: "),
            pass_only: yes_or_no("Ungraded"),
        }
    }
}
