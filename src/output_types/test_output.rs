use serde::Serialize;

#[derive(Debug, Eq, PartialEq, Serialize, Default, Clone, PartialOrd, Hash, Ord)]
pub struct TestOutput {
    pub name: String,
    pub annotation: String,
    pub label: String,
}
