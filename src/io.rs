use std::fmt::Display;
use std::str::FromStr;
use std::sync::Mutex;

use anyhow::Result;
use once_cell::sync::Lazy;
use rustyline::completion::{Completer, FilenameCompleter, Pair};
use rustyline::error::ReadlineError;
use rustyline::highlight::Highlighter;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::{Config, Context, Editor};
use rustyline_derive::{Helper, Validator};

#[derive(Helper, Validator)]
pub struct MyHelper {
    completer: FilenameCompleter,
    hinter: HistoryHinter,
}

impl Completer for MyHelper {
    type Candidate = Pair;

    fn complete(
        &self,
        line: &str,
        pos: usize,
        ctx: &Context<'_>,
    ) -> Result<(usize, Vec<Pair>), ReadlineError> {
        self.completer.complete(line, pos, ctx)
    }
}

impl Highlighter for MyHelper {}

impl Hinter for MyHelper {
    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.hinter.hint(line, pos, ctx)
    }
}

pub static RL: Lazy<Mutex<Editor<MyHelper>>> = Lazy::new(|| {
    let config = Config::builder().auto_add_history(true).build();
    let mut rl = Editor::with_config(config);
    let helper = MyHelper {
        completer: FilenameCompleter::new(),
        hinter: HistoryHinter {},
    };
    rl.set_helper(Some(helper));
    Mutex::new(rl)
});

pub fn input<F, P: AsRef<str>>(prompt: P) -> F
where
    F: FromStr,
    F::Err: Display,
{
    let line = RL
        .lock()
        .expect("Unable to lock static RL")
        .readline(prompt.as_ref())
        .unwrap_or_else(|err| panic!("IO Error: {}", err));

    let parsed: Result<F, F::Err> = line.trim().parse();
    match parsed {
        Err(e) => {
            log::error!("Error parsing: {}\n{}", line, e);
            input(prompt)
        }
        Ok(val) => val,
    }
}

pub fn yes_or_no<P: AsRef<str>>(prompt: P) -> bool {
    let allowed_responses = ["y", "Y", "n", "N", ""];
    let mut buffer: String;
    while {
        buffer = input(format!("{}, (Y/n): ", prompt.as_ref()).as_str());
        !allowed_responses.contains(&buffer.as_str())
    } {}
    match buffer.as_str() {
        "y" | "Y" | "" => true,
        "n" | "N" => false,
        &_ => unreachable!(),
    }
}
