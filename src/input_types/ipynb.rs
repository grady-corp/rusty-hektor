use core::fmt;
use std::collections::BTreeMap;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io::{Read, Write};
use std::path::PathBuf;
use std::process::Command;
use std::str::FromStr;

use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::io::{input, yes_or_no};

/// Media attachments (e.g. inline images), stored as mimebundle keyed by filename.
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct Attachments(BTreeMap<String, Mimebundle>);

/// A mime-type keyed dictionary of data
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct Mimebundle(BTreeMap<String, MultilineString>);

impl Display for Mimebundle {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Some(content) = self.0.get("text/plain") {
            write!(f, "{}", content)
        } else {
            write!(
                f,
                "Unsupported display type, please refer to source notebook."
            )
        }
    }
}

/// Contents of the cell, represented as an array of lines.
pub type Source = MultilineString;

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum MultilineString {
    SingleString(String),
    MultiLine(Vec<String>),
}

impl Display for MultilineString {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use MultilineString::*;

        match self {
            SingleString(single) => write!(f, "{}", single),
            MultiLine(multi) => write!(f, "{}", multi.join("")),
        }
    }
}

impl From<MultilineString> for String {
    fn from(s: MultilineString) -> Self {
        match s {
            MultilineString::SingleString(single) => single,
            MultilineString::MultiLine(multi) => multi.join("\n"),
        }
    }
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct Notebook {
    /// Array of cells of the current notebook.
    pub cells: Vec<Cell>,
    /// Notebook format (major number). Incremented between backwards incompatible changes to the
    /// notebook format.
    pub nbformat: i64,
    /// Notebook format (minor number). Incremented for backward compatible changes to the notebook
    /// format.
    pub nbformat_minor: i64,
}

impl FromStr for Notebook {
    type Err = NotebookParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match serde_json::from_str(s) {
            Err(err) => {
                println!(
                    "An error has been encountered while parsing a notebook:\n{}",
                    err
                );
                let edit = yes_or_no("Edit the file in an editor to fix the json?:");
                if !edit {
                    return Err(err.into());
                }
                let fixed_json = edit_json(s);
                fixed_json.parse()
            }
            Ok(notebook) => Ok(notebook),
        }
    }
}

fn edit_json(s: &str) -> String {
    let editor = get_editor();
    let mut file = tempfile::NamedTempFile::new().expect("Unable to create temp file");
    file.write_all(s.as_bytes())
        .expect("Unable to write json to temp file");
    Command::new(editor)
        .arg(file.as_ref())
        .status()
        .expect("Unable to start editor");
    let mut buf = String::new();
    let mut file = file.reopen().expect("Unable to reopen temp file");
    file.read_to_string(&mut buf)
        .expect("Unable to read from temp file");
    buf
}

fn get_editor() -> PathBuf {
    let editor: PathBuf = input("Please enter a name or a path to a terminal editor: ");
    editor
}

impl Display for Notebook {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for cell in &self.cells {
            write!(f, "{}", cell)?;
        }
        Ok(())
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
#[serde(tag = "cell_type", rename_all = "lowercase")]
pub enum Cell {
    Code(CodeCell),
    Markdown(MarkdownCell),
    Raw(RawCell),
}

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Code(cell) => write!(f, "{}", cell),
            Self::Markdown(cell) => write!(f, "{}", cell),
            Self::Raw(cell) => write!(f, "{}", cell),
        }
    }
}

/// Notebook code cell.
#[serde(rename = "code_cell")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct CodeCell {
    /// Execution, display, or stream outputs.
    pub outputs: Vec<Output>,
    pub source: Source,
    /// The code cell's prompt number. Will be null if the cell has not been run.
    #[serde(default)]
    pub execution_count: Option<i64>,
}

const MAX_OUTPUT_LINES: usize = 50;

impl Display for CodeCell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let exec_count_as_str = self
            .execution_count
            .map(|count| count.to_string())
            .unwrap_or_else(|| " ".into());
        writeln!(f, "\n# In[{}]:\n", exec_count_as_str)?;
        writeln!(f, "{}\n", self.source)?;

        writeln!(f, "# Out[{}]:\n", exec_count_as_str)?;

        let mut has_stream_output = false;
        for output in &self.outputs {
            let mut out_str = output.to_string();

            // IPython batches stream outputs if they are too long
            // we only want to show the first batch of stream outputs.
            // Also, we want to truncate too long output streams.
            if let Output::Stream(stream) = output {
                if has_stream_output {
                    continue;
                }
                has_stream_output = true;

                out_str = stream
                    .to_string()
                    .lines()
                    .take(MAX_OUTPUT_LINES)
                    .collect::<Vec<_>>()
                    .join("\n");

                if out_str.lines().count() == MAX_OUTPUT_LINES {
                    out_str.push_str("\nOUTPUT HAS BEEN TRUNCATED. SEE ORIGINAL NOTEBOOK FOR FULL OUTPUT.");
                }
            }

            writeln!(f, "{}", comment_out(out_str))?;
        }
        Ok(())
    }
}

/// Notebook markdown cell.
#[serde(rename = "markdown_cell")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct MarkdownCell {
    pub attachments: Option<Attachments>,
    pub source: Source,
}

impl Display for MarkdownCell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "\n# MarkdownCell:\n{}",
            comment_out(self.source.to_string())
        )?;
        if self.attachments.is_some() {
            write!(
                f,
                "# Cell contains attachments, please refer to source notebook"
            )?;
        }
        Ok(())
    }
}

/// Notebook raw nbconvert cell.
#[serde(rename = "raw_cell")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RawCell {
    pub attachments: Option<Attachments>,
    pub source: Source,
}

impl Display for RawCell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "\n# RawCell:\n{}", comment_out(self.source.to_string()))?;
        if self.attachments.is_some() {
            writeln!(
                f,
                "# Cell contains attachments, please refer to source notebook"
            )?;
        }
        Ok(())
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
#[serde(tag = "output_type")]
#[allow(non_camel_case_types)]
pub enum Output {
    #[serde(rename = "execute_result")]
    Execute(ExecuteResult),
    #[serde(rename = "display_data")]
    Display(DisplayData),
    #[serde(rename = "stream")]
    Stream(Stream),
    #[serde(rename = "error")]
    Error(ExecutionError),
}

impl Display for Output {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Execute(out) => write!(f, "{}", out),
            Self::Display(out) => write!(f, "{}", out),
            Self::Stream(out) => write!(f, "{}", out),
            Self::Error(out) => write!(f, "{}", out),
        }
    }
}

/// Result of executing a code cell.
#[serde(rename = "execute_result")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct ExecuteResult {
    pub data: Mimebundle,
    #[serde(default)]
    pub execution_count: Option<i64>,
}

impl Display for ExecuteResult {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

/// Data displayed as a result of code cell execution.
#[serde(rename = "display_data")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct DisplayData {
    pub data: Mimebundle,
}

impl Display for DisplayData {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.data)
    }
}

/// Stream output from a code cell.
#[serde(rename = "stream")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct Stream {
    /// The name of the stream (stdout, stderr).
    pub name: String,
    /// The stream's text output, represented as an array of strings.
    pub text: MultilineString,
}

impl Display for Stream {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}

static STRIP_ASCII_COLOR: Lazy<Regex> =
    Lazy::new(|| Regex::new("\u{001B}\\[[;\\d]*[ -/]*[@-~]").unwrap());

/// Output of an error that occurred during code cell execution.
#[serde(rename = "error")]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct ExecutionError {
    /// The name of the error.
    pub ename: String,
    /// The value, or message, of the error.
    pub evalue: String,
    /// The error's traceback, represented as an array of strings.
    pub traceback: Vec<String>,
}

impl Display for ExecutionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "ERROR: {} {}\n", self.ename, self.evalue)?;
        //TODO strip colouring from traceback
        let traceback = self.traceback.join("\n");
        let traceback = STRIP_ASCII_COLOR.replace_all(&traceback, "");
        writeln!(f, "Traceback:\n{}", traceback)
    }
}

fn comment_out(input: impl AsRef<str>) -> String {
    input
        .as_ref()
        .split('\n')
        .map(|line| format!("# {}\n", line))
        .collect()
}

#[derive(Debug)]
pub struct NotebookParseError {
    source: serde_json::error::Error,
}

impl Error for NotebookParseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.source)
    }
}

impl fmt::Display for NotebookParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        writeln!(
            f,
            "Failed to parse Notebook. Source:\n{}",
            self.source().unwrap()
        )
    }
}

impl From<serde_json::Error> for NotebookParseError {
    fn from(err: serde_json::Error) -> Self {
        Self { source: err }
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::BufReader;

    use super::*;

    #[test]
    fn can_parse_example_notebook() {
        let reader = BufReader::new(File::open("src/input_types/test_data/example.ipynb").unwrap());
        let _data: Notebook = serde_json::from_reader(reader).unwrap();
    }
}
