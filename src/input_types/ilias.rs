use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use quick_xml::de::from_reader;
use regex::Regex;
use serde::Deserialize;
use zip::ZipArchive;

use anyhow::{anyhow, Context, Result};

use crate::input_types::ilias::qti_file::QtiData;
use crate::input_types::ilias::results_file::Results;

pub struct IliasExportData {
    pub qti_data: QtiData,
    pub results_data: Results,
}

pub fn from_zip(path: impl AsRef<Path>) -> Result<IliasExportData> {
    let file = File::open(path)?;
    let mut archive = ZipArchive::new(file)?;

    let mut qti_data = None;
    let mut results_data = None;
    for idx in 0..archive.len() {
        let zipped_file = archive
            .by_index(idx)
            .context("Used wrong index for archive")?;
        if !zipped_file.is_file() {
            continue;
        }
        let name = zipped_file.name();
        if name.contains("qti") {
            let parsed =
                from_reader(BufReader::new(zipped_file)).context("Unable to parse qti data")?;
            qti_data = Some(parsed);
        } else if name.contains("results") {
            let parsed =
                from_reader(BufReader::new(zipped_file)).context("Unable to parse results data")?;
            results_data = Some(parsed);
        }
    }
    let (qti_data, results_data) = match (qti_data, results_data) {
        (Some(qti_data), Some(results_data)) => (qti_data, results_data),
        _ => return Err(anyhow!(
            "Provided zip archive needs to contain qti and results file"
        )),
    };

    Ok(IliasExportData {
        qti_data,
        results_data,
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_zip() {
        from_zip("src/input_types/test_data/test.zip").unwrap();
    }
}

pub mod results_file {
    use super::*;

    #[derive(Deserialize, Debug)]
    pub struct Results {
        pub tst_active: TstActive,
        pub tst_solutions: TstSolutions,
    }

    #[derive(Deserialize, Debug)]
    pub struct TstActive {
        #[serde(rename = "row")]
        pub users: Vec<User>,
    }

    #[derive(Deserialize, Debug)]
    pub struct User {
        pub active_id: String,
        pub matr_nr: Option<String>,
        pub fullname: String,
    }

    #[derive(Deserialize, Debug)]
    pub struct TstSolutions {
        #[serde(rename = "row")]
        pub solutions: Vec<Solution>,
    }

    #[derive(Deserialize, Debug, Eq, PartialEq)]
    pub struct Solution {
        pub active_fi: String,
        pub question_fi: String,
        pub value1: String,
        pub value2: String,
        pub tstamp: u64,
    }

    #[cfg(test)]
    mod tests {
        use crate::input_types::from_path;

        use super::*;

        #[test]
        fn can_parse_results() {
            let _data: Results = from_path("src/input_types/test_data/results.xml").unwrap();
        }
    }
}

pub mod qti_file {
    use once_cell::sync::Lazy;
    use sxd_document::{parser, writer::format_document};
    use sxd_xpath::{Context, Factory, Value};

    use anyhow::{anyhow, Result};

    use super::*;

    pub static ID_RE: Lazy<Regex> =
        Lazy::new(|| Regex::new(r"il_\d+_qst_(?P<sub_type_id>\d+)").unwrap());

    #[derive(Deserialize, Debug)]
    pub struct QtiData {
        pub assessment: Assessment,
    }

    #[derive(Deserialize, Debug)]
    pub struct Assessment {
        pub section: Section,
        #[serde(rename = "qtimetadata")]
        pub qti_meta_data: QtiMetaData
    }

    #[derive(Deserialize, Debug)]
    pub struct Section {
        pub ident: String,
        #[serde(rename = "item")]
        pub items: Vec<Item>,
    }

    #[derive(Deserialize, Debug)]
    pub struct Item {
        pub ident: String,
        pub title: String,
        #[serde(rename = "itemmetadata")]
        pub item_meta_data: ItemMetaData,
        pub presentation: Presentation,
    }

    #[derive(Deserialize, Debug)]
    pub struct ItemMetaData {
        #[serde(rename = "qtimetadata")]
        pub qti_meta_data: QtiMetaData,
    }

    #[derive(Deserialize, Debug)]
    pub struct QtiMetaData {
        #[serde(rename = "qtimetadatafield")]
        pub meta_data_fields: Vec<MetaDataField>,
    }

    #[derive(Deserialize, Debug)]
    pub struct MetaDataField {
        #[serde(rename = "fieldlabel")]
        pub field_label: String,
        #[serde(rename = "fieldentry")]
        pub field_entry: String,
    }

    #[derive(Deserialize, Debug)]
    pub struct Presentation {
        pub flow: Flow,
    }

    #[derive(Deserialize, Debug)]
    pub struct Flow {
        #[serde(rename = "$value")]
        pub materials: Vec<Material>,
    }

    // See https://github.com/tafia/quick-xml/issues/177 for why this workaround is necessary
    #[derive(Deserialize, Debug)]
    pub enum Material {
        #[serde(rename = "material")]
        Material {
            #[serde(rename = "mattext")]
            mat_text: MaterialText,
        },
        #[serde(other)]
        Other,
    }

    #[derive(Deserialize, Debug)]
    pub struct MaterialText {
        #[serde(rename = "texttype")]
        pub text_type: String,
        #[serde(rename = "$value")]
        pub text: Option<String>,
    }

    /// This function is used to preprocess the html of a description in order to
    /// render the latex in the frontend.
    /// Every span of class="latex" is wrapped in '()'
    pub fn preprocess_material_text_latex(text: String) -> Result<String> {
        let context = Context::new();
        let factory = Factory::new();
        let latex_xp = factory.build("//span[@class='latex']").unwrap().unwrap();
        let wrapped_text = format!("<div>{}</div>", text);
        let package = parser::parse(&wrapped_text)?;
        let doc = package.as_document();
        let items = match latex_xp.evaluate(&context, doc.root())? {
            Value::Nodeset(nodes) => nodes,
            _ => return Err(anyhow!("latex xpath didn't return nodeset")),
        };
        for item in items.iter() {
            match item.element() {
                Some(el) => {
                    el.set_text(&format!("\\({}\\)", item.string_value()));
                }
                None => log::warn!(
                    "Encountered Node without Element during Latex preprocessing.\
                     Output may not be able to be rendered"
                ),
            }
        }
        let mut buf = vec![];
        format_document(&doc, &mut buf)?;
        Ok(String::from_utf8(buf)?)
    }

    #[cfg(test)]
    mod tests {
        use crate::input_types::from_path;

        use super::*;

        #[test]
        fn can_parse_qti() {
            let _data: QtiData = from_path("src/input_types/test_data/qti.xml").unwrap();
        }
    }
}

pub mod results_csv {
    use std::collections::HashMap;
    use std::path::Path;

    use anyhow::Result;

    use super::*;

    #[derive(Deserialize, Debug)]
    pub struct ResultsCsvRow {
        #[serde(rename = "Name")]
        name: String,
        #[serde(rename = "Benutzername")]
        username: String,
    }

    /// maps participants names to their username
    pub type ResultsCsvMapping = HashMap<String, String>;

    pub fn results_mapping_from_path(path: impl AsRef<Path>) -> Result<ResultsCsvMapping> {
        let mut csv_reader = csv::ReaderBuilder::new().delimiter(b';').from_path(path)?;
        let mut mapping = HashMap::new();
        for record in csv_reader.deserialize() {
            let record: ResultsCsvRow = record?;
            let name = record.name;
            match mapping.get(&name) {
                None => {
                    mapping.insert(name, record.username);
                }
                Some(_) => {
                    log::error!(
                        "Student \"{}\" appears twice in studip results export. Username will be the same \
                         for persons with this name. FIX MANUALLY IN OUTPUT FILE!",
                        name
                    );
                }
            }
        }
        Ok(mapping)
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        fn init() {
            let _ = env_logger::builder().is_test(true).try_init();
        }

        #[test]
        fn can_parse_results_csv() {
            init();
            let _data = results_mapping_from_path("src/input_types/test_data/results.csv").unwrap();
        }
    }
}
