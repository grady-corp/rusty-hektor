use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt::Display;
use std::fs;
use std::path::PathBuf;
use std::str::FromStr;

use anyhow::{Context, Result};

use crate::input_types::ilias::qti_file::Material;
use crate::input_types::ilias::qti_file::{
    self, preprocess_material_text_latex, Item, MaterialText, QtiData,
};
use crate::io::{input, yes_or_no};
use crate::output_types::submission_type::{ProgrammingLang, SubmissionType};
use crate::transform::TransformContext;

pub fn transform_sub_types(
    ilias_qti_data: &QtiData,
    context: &TransformContext,
) -> Result<HashMap<String, SubmissionType>> {
    let allowed_question_types = if context.parse_cloze_questions.is_some() {
        ["CLOZE QUESTION"]
    } else {
        ["assSourceCode"]
    };

    let sub_types = {
        let mut map = HashMap::new();
        for item in &ilias_qti_data.assessment.section.items {
            if let Some((id, sub_type)) =
                transform_sub_type(item, &allowed_question_types, context)?
            {
                map.insert(id, sub_type);
            }
        }
        map
    };
    Ok(sub_types)
}

fn transform_sub_type(
    qti_item: &Item,
    allowed_question_types: &[&str],
    context: &TransformContext,
) -> Result<Option<(String, SubmissionType)>> {
    let sub_type_id = qti_file::ID_RE
        .captures(&qti_item.ident)
        .context("Unable to parse question id from qti data")?
        .name("sub_type_id")
        .context("Unable to parse question id from qti data")?
        .as_str()
        .to_owned();
    let name = &qti_item.title;

    let meta_fields: HashMap<String, String> = qti_item
        .item_meta_data
        .qti_meta_data
        .meta_data_fields
        .iter()
        .map(|field| (field.field_label.clone(), field.field_entry.clone()))
        .collect();
    // skip transformation for this item if question type is not parsed
    match meta_fields.get(MetaField::QuestionType.map_index()) {
        Some(val) if !allowed_question_types.contains(&val.as_str()) => return Ok(None),
        _ => (),
    }

    // When a sub types map is provided we take the corresponding values for solution, full_score
    // and lang from the map, provided that the name of the current sub type is contained.
    // Otherwise the data is taken from the xml data/input
    let (solution, full_score, programming_language) =
        if let Some(sub_types) = context.sub_types_by_name.as_ref() {
            let sub_type = sub_types.get(name).with_context(|| {
                format!(
                    "Passed sub types as context but \"{}\" is not contained",
                    &name
                )
            })?;
            (
                sub_type.solution.clone(),
                sub_type.full_score,
                sub_type.programming_language,
            )
        } else if context.no_user_input {
          Default::default()
        } else{

            let solution = if yes_or_no(format!(
                "Add sample solution for \"{}\" by providing path to source file?",
                &name
            )) {
                loop {
                    let path: PathBuf =
                        input("Where is the solution stored? (Tab file completion works): ");
                    match fs::read_to_string(path) {
                        Err(e) => log::error!("Error reading solution file:\n{}", e),
                        Ok(file_content) => break file_content,
                    };
                }
            } else {
                "No solution available.".into()
            };

            let full_score: u32 = get_field(&meta_fields, MetaField::FullScore, &name);
            let programming_language: ProgrammingLang =
                get_field(&meta_fields, MetaField::ProgrammingLang, &name);

            (solution, full_score, programming_language)
        };

    // This only takes the first material since it is unclear how to concatenate multiple ones
    let material = qti_item
        .presentation
        .flow
        .materials
        .iter()
        .filter_map(|mat| match mat {
            Material::Material { mat_text } => Some(mat_text),
            Material::Other => None,
        })
        .next();
    let description = match material {
        Some(MaterialText {
            text_type,
            text: Some(text),
        }) => {
            if text_type == "text/xhtml" && context.enable_latex_rendering {
                preprocess_material_text_latex(text.clone())?
            } else {
                text.clone()
            }
        }
        _ => "No description available.".into(),
    };

    let sub_type = SubmissionType {
        name: String::from(name),
        full_score,
        description,
        solution,
        programming_language,
    };

    Ok(Some((sub_type_id, sub_type)))
}

enum MetaField {
    FullScore,
    ProgrammingLang,
    QuestionType,
}

impl MetaField {
    fn map_index(&self) -> &str {
        match self {
            Self::FullScore => "POINTS",
            Self::ProgrammingLang => "SOURCE_CODE_LANG",
            Self::QuestionType => "QUESTIONTYPE",
        }
    }

    fn readable(&self) -> Cow<str> {
        match self {
            Self::FullScore => "full score".into(),
            Self::ProgrammingLang => format!(
                "programming language (Available: {})",
                ProgrammingLang::list_available()
            )
            .into(),
            Self::QuestionType => "question type".into(),
        }
    }
}

fn get_field<T>(meta_fields: &HashMap<String, String>, field: MetaField, sub_type_name: &str) -> T
where
    T: FromStr + Display,
    T::Err: Display,
{
    let enter_field = || {
        input(format!(
            "Enter {} for \"{}\": ",
            field.readable(),
            sub_type_name
        ))
    };
    let field_val: T = match meta_fields.get(field.map_index()) {
        None => enter_field(),
        Some(field_val) => {
            let field_val: std::result::Result<T, _> = field_val.parse();
            match field_val {
                Err(_) => enter_field(),
                Ok(field_val) => {
                    if yes_or_no(format!(
                        "Set {} as {} for \"{}\"",
                        field_val,
                        field.readable(),
                        sub_type_name
                    )) {
                        field_val
                    } else {
                        enter_field()
                    }
                }
            }
        }
    };

    field_val
}

#[cfg(test)]
mod tests {
    //    use crate::input_types::from_path;
    //    use super::*;

    #[test]
    fn test_transform_sub_types() {
        //        let data: QtiData = from_path("src/input_types/test_data/qti.xml").unwrap();
        //        let transformed_data = transform_sub_types(data, &TransformContext::default()).unwrap();
        //        panic!("{:?}", transformed_data)
    }
}
