use std::collections::{HashMap, HashSet};

use anyhow::{anyhow, Result};

use crate::input_types::ilias::results_csv::ResultsCsvMapping;
use crate::input_types::various::EmailGroupMapping;
use crate::input_types::IliasExportData;
use crate::output_types::exam::Exam;
use crate::output_types::meta::MetaInformation;
use crate::output_types::module::Module;
use crate::output_types::student::{Student, StudentBuilder};
use crate::output_types::submission::Submission;
use crate::output_types::submission_type::SubmissionType;
use crate::output_types::HektorOutput;
use crate::transform::students::transform_students;
use crate::transform::sub_types::transform_sub_types;
use crate::transform::submissions::transform_submissions;

pub mod students;
pub mod sub_types;
pub mod submissions;

pub struct TransformContext {
    pub results_csv: Option<ResultsCsvMapping>,
    pub email_group_mapping: Option<EmailGroupMapping>,
    /// for Some(i) parse the i'th field of a cloze question as source code
    /// for None only parse solutions for question of type "assSourceCode"
    pub parse_cloze_questions: Option<u32>,
    pub enable_latex_rendering: bool,
    pub sub_types_by_name: Option<HashMap<String, SubmissionType>>,
    // Use default values in place of user input
    pub no_user_input: bool,
}

impl Default for TransformContext {
    fn default() -> Self {
        TransformContext {
            results_csv: None,
            email_group_mapping: None,
            parse_cloze_questions: None,
            enable_latex_rendering: true,
            sub_types_by_name: None,
            no_user_input: true,
        }
    }
}

pub fn transform(
    ilias_export: IliasExportData,
    context: &TransformContext,
) -> Result<HektorOutput> {
    let submission_types = transform_sub_types(&ilias_export.qti_data, context)?;
    let students = transform_students(ilias_export.results_data.tst_active.users, context)?;
    let submissions = transform_submissions(
        ilias_export.results_data.tst_solutions.solutions,
        &submission_types,
        context,
    )?;

    let students = merge_student_with_submissions(students, submissions);
    let module = if context.no_user_input {
        Module::default()
    } else {
        Module::from_input()
    };
    let meta = MetaInformation::new();
    let submission_types = submission_types
        .into_iter()
        .map(|(_key, val)| val)
        .collect();
    let start_timestamp = match ilias_export.qti_data.assessment.qti_meta_data.meta_data_fields
        .into_iter()
        .find(|f| f.field_label == "activation_start_time") {
        Some(t) => t.field_entry.parse::<u32>()?,
        _ => {
            log::warn!("Could not get exam date. Using 0 as default value");
            0
        }
    };
    let exam = Exam {
        module,
        exam_start_timestamp: start_timestamp,
        submission_types,
        students,
    };

    Ok(HektorOutput { meta, data: exam })
}

pub fn merge_transform(
    mut ilias_exports: Vec<IliasExportData>,
    context: &mut TransformContext,
) -> Result<HektorOutput> {
    assert!(
        ilias_exports.len() >= 2,
        "Can only merge_transform when passed multiple export data"
    );
    assert!(
        context.sub_types_by_name.is_none(),
        "merge_transform must not be called with sub_types in context"
    );
    let last = ilias_exports.pop().unwrap();
    let submission_types = transform_sub_types(&last.qti_data, context)?;
    let mut students = transform_students(last.results_data.tst_active.users, context)?;
    let mut submissions = transform_submissions(
        last.results_data.tst_solutions.solutions,
        &submission_types,
        context,
    )?;

    // Since we only export one exam, we take the start date of the first exam
    let first_export = match ilias_exports.iter().next() {
        Some(t) => t,
        _ => unreachable!("Need to provide at least one export file")
    };
    let start_timestamp = match first_export.qti_data.assessment.qti_meta_data.meta_data_fields
        .iter()
        .find(|f| f.field_label == "activation_start_time") {
        Some(t) => t.field_entry.parse::<u32>()?,
        _ => {
            log::warn!("Could not get exam date. Using 0 as default value");
            0
        }
    };

    let sub_types_set: HashSet<SubmissionType> = submission_types.values().cloned().collect();
    let sub_types_by_name = submission_types
        .values()
        .cloned()
        .map(|s| (s.name.clone(), s))
        .collect();
    context.sub_types_by_name = Some(sub_types_by_name);
    for other_export in ilias_exports {
        // When parsing multiple exports we need to check that the passed export files have
        // compatible submission types. If there is **any** difference in the submission types
        // a warning is issued (e.g. when the description is different). Because this might
        // and already has occurred, the program is not aborted and the first passed
        // submission types are included in the output. Should however the names of the submission
        // types differ this is likely due to passing the wrong file and an error is returned
        let other_sub_types = transform_sub_types(&other_export.qti_data, context)?;
        let other_sub_types_set = other_sub_types.values().cloned().collect();
        if sub_types_set != other_sub_types_set {
            let symmetric_diff: Vec<_> = sub_types_set
                .symmetric_difference(&other_sub_types_set)
                .collect();
            let sub_types_names: HashSet<_> = submission_types
                .values()
                .map(|sub_type| &sub_type.name)
                .collect();
            let other_sub_types_names: HashSet<_> = other_sub_types
                .values()
                .map(|sub_type| &sub_type.name)
                .collect();
            let name_symmetric_diff: Vec<_> = sub_types_names
                .symmetric_difference(&other_sub_types_names)
                .collect();
            if name_symmetric_diff.is_empty() {
                log::warn!(
                    "Different submission types with same name in exports. \
                The following are contained in one but not the other:\n {:#?}",
                    symmetric_diff
                )
            } else {
                return Err(anyhow!(
                    "Differently named submission types in exports. \
            The following are contained in one but not the other:\n {:?}",
                    name_symmetric_diff
                ));
            }
        }

        let other_students =
            transform_students(other_export.results_data.tst_active.users, context)?;
        students.reserve(other_students.len());
        for (key, student) in other_students {
            if let Some(student) = students.insert(key, student) {
                return Err(anyhow!(
                    "Student {:?} is contained in two differing exports.",
                    student
                ));
            }
        }
        let other_submissions = transform_submissions(
            other_export.results_data.tst_solutions.solutions,
            &other_sub_types,
            context,
        )?;
        submissions.reserve(other_submissions.len());
        for (key, submission) in other_submissions {
            if submissions.insert(key, submission).is_some() {
                return Err(anyhow!(
                    "Submissions for student with equal active id \
                are contained in two differing exports."
                ));
            }
        }
    }

    let students = merge_student_with_submissions(students, submissions);

    let module = if context.no_user_input {
        Module::default()
    } else {
        Module::from_input()
    };
    let meta = MetaInformation::new();
    let submission_types = submission_types
        .into_iter()
        .map(|(_key, val)| val)
        .collect();
    let exam = Exam {
        module,
        exam_start_timestamp: start_timestamp,
        submission_types,
        students,
    };

    Ok(HektorOutput { meta, data: exam })
}

fn merge_student_with_submissions(
    students: HashMap<String, StudentBuilder>,
    mut submissions: HashMap<String, Vec<Submission>>,
) -> Vec<Student> {
    students
        .into_iter()
        .map(|(active_id, mut student_builder)| {
            let submissions_for_stud = match submissions.remove(&active_id) {
                Some(subs) => subs,
                None => {
                    log::warn!(
                        "No submissions for student '{:?}', please double check in ILIAS",
                        student_builder
                    );
                    vec![]
                }
            };
            student_builder.submissions(submissions_for_stud);
            student_builder
                .build()
                .expect("BUG: student_builder not properly initialized")
        })
        .collect()
}

#[cfg(test)]
mod tests {
    //    use crate::input_types::ilias::from_zip;
    //    use crate::transform::{transform, TransformContext};

    #[test]
    fn test_transform() {
        //TODO this test is not executable at the moment since transforming submission types
        // and Module::from_input() is dependent on user input. Possible solution would
        // be to pass default values in context
        //
        //        let ilias_data = from_zip("src/input_types/test_data/test.zip").unwrap();
        //        let context = TransformContext::default();
        //        transform(ilias_data, &context).unwrap();
    }
}
