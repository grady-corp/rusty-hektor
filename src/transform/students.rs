use std::collections::HashMap;

use anyhow::{Context, Result};

use crate::input_types::ilias::results_file::User;
use crate::output_types::student::StudentBuilder;
use crate::transform::TransformContext;
use once_cell::sync::OnceCell;

pub fn transform_students(
    users: Vec<User>,
    context: &TransformContext,
) -> Result<HashMap<String, StudentBuilder>> {
    users
        .into_iter()
        .map(|user| transform_student(user, context))
        .collect()
}

pub fn transform_student(
    user: User,
    context: &TransformContext,
) -> Result<(String, StudentBuilder)> {
    // Use static to only issue one warning when matr_nr not present in order to not clutter
    // terminal
    static ISSUED_WARNING: OnceCell<()> = OnceCell::new();
    let mut builder = StudentBuilder::default();
    let active_id = user.active_id;
    let fullname = user.fullname;

    let identifier = user.matr_nr.unwrap_or_else(|| {
        if ISSUED_WARNING.get().is_some() {
            log::info!(
                "No matr_nr for student \"{}\", falling back to active_id of xml data",
                &fullname
            );
        } else {
            ISSUED_WARNING.set(()).expect("Set once cell");
            log::warn!(
                "No matr_nr for student \"{}\", falling back to active_id of xml data. \
                There are likely more students without matr_nr, run with RUST_LOG=info \
                to see further log output.",
                &fullname
            );
        }
        active_id.clone()
    });
    builder.identifier(identifier);

    let username = match &context.results_csv {
        Some(mapping) => mapping
            .get(&fullname)
            .with_context(|| {
                format!(
                    "Student with name \"{}\" not contained in passed ILIAS results csv",
                    &fullname
                )
            })?
            .clone(),
        None => create_username(&fullname, &active_id),
    };

    builder.username(username);

    if let Some(group_email_mapping) = &context.email_group_mapping {
        if let Some(student_data) = group_email_mapping.get(&fullname) {
            builder.email(student_data.email.clone());
            builder.exercise_groups(vec![student_data.exercise_group.clone()]);
        } else {
            builder.email("".into());
            builder.exercise_groups(vec!["No group".into()]);
            log::error!(
                "Provided group and email mapping data, but student \"{}\" does not \
                 appear in it. Email and group will be blank.",
                &fullname
            );
        }
    }
    builder.fullname(fullname);

    Ok((active_id, builder))
}

fn create_username(fullname: &str, identifier: &str) -> String {
    fullname
        .chars()
        .filter(|c| c.is_uppercase())
        .chain(identifier.chars())
        .collect()
}
