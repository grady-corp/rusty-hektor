use std::collections::HashMap;

use itertools::Itertools;

use anyhow::{Context, Result};

use crate::input_types::ilias::results_file::Solution;
use crate::input_types::ipynb::Notebook;
use crate::output_types::submission::Submission;
use crate::output_types::submission_type::{ProgrammingLang, SubmissionType};
use crate::transform::TransformContext;
use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

/// Given an unprocessed vector of solutions (coming from the results.xml file)
/// and some additional data, extract the submissions for each user.
///
/// ## Returns
/// A HashMap mapping a users active_id to a vector of their submissions
pub fn transform_submissions(
    solutions: Vec<Solution>,
    submission_types: &HashMap<String, SubmissionType>,
    context: &TransformContext,
) -> Result<HashMap<String, Vec<Submission>>> {
    // maps a solutions active_fi (the user id) and the question_fi (id of the question) to
    // a vector of solutions (there might be multiple with different timestamps)
    let mut solution_map: HashMap<(String, String), Vec<Solution>> = HashMap::new();

    for solution in solutions {
        if submission_types.contains_key(&solution.question_fi) {
            solution_map
                .entry((solution.active_fi.clone(), solution.question_fi.clone()))
                .or_insert_with(|| vec![])
                .push(solution);
        }
    }

    // maps a users active_fi to his Submissions
    let mut submission_map = HashMap::new();

    let create_submission = |question_fi: &str, code| -> Result<Submission> {
        let sub_type = submission_types
            .get(question_fi)
            .expect("BUG: Encountered solution with unknown question_fi");

        let type_name = sub_type.name.clone();
        let mut submission = Submission {
            code,
            source_code: None,
            type_name,
            tests: vec![],
        };

        if sub_type.programming_language == ProgrammingLang::Python && submission.code.len() > 0 {
            render_code::<Notebook>(&mut submission)?;
        }
        Ok(submission)
    };

    for ((active_fi, question_fi), mut solutions) in solution_map {
        // substract timestamp from MAX to sort values in **descending** order
        solutions.sort_by_key(|solution| std::u64::MAX - solution.tstamp);
        if let Some((_tst, mut oldest_group)) = solutions
            .into_iter()
            .group_by(|solution| solution.tstamp)
            .into_iter()
            .next()
        {
            if let Some(field) = context.parse_cloze_questions {
                // This code works under the assumption, that for cloze type questions
                // one solution entry is generated for each field where the value of
                // `solution.value1` denotes the field and that all these solution have the same
                // timestamp
                match oldest_group.find(|solution: &Solution| solution.value1 == field.to_string())
                {
                    Some(solution) => {
                        let code = String::from_utf8(
                            base64::decode(&solution.value2).with_context(|| {
                                format!(
                                    "base64 decoding failed for field2 of solution: {:#?}",
                                    solution
                                )
                            })?,
                        )?;
                        let submission = create_submission(&question_fi, code)?;
                        submission_map
                            .entry(active_fi)
                            .or_insert_with(|| vec![])
                            .push(submission);
                    }
                    None => {
                        log::warn!(
                            "Unable to find solution with field={}, for active_fi={}, question_fi={}. \
                            Falling back to empty solution.",
                            field,
                            active_fi,
                            question_fi
                        );

                        let submission = create_submission(&question_fi, String::from(""))?;
                        submission_map
                            .entry(active_fi)
                            .or_insert_with(|| vec![])
                            .push(submission);
                    }
                }
            } else {
                // in this branch we're parsing question of type "assSourceCode"
                // which is mutually exclusive with the cloze question type
                let solution = oldest_group
                    .next()
                    .expect("BUG: Encountered empty oldest_group");
                for other_solution in oldest_group {
                    assert_eq!(
                        &solution, &other_solution,
                        "Unexpectedly encountered group of solutions for question \
                     with type `assSourceCode` that has differing solutions. \
                     What the heck is wrong with this ILIAS format?!"
                    )
                }
                let code =
                    String::from_utf8(base64::decode(&solution.value1).with_context(|| {
                        format!(
                            "base64 decoding failed for field1 of solution: {:#?}",
                            solution
                        )
                    })?)?;

                let submission = create_submission(&question_fi, code)?;
                submission_map
                    .entry(active_fi)
                    .or_insert_with(|| vec![])
                    .push(submission);
            }
        }
    }

    Ok(submission_map)
}

fn render_code<T>(submission: &mut Submission) -> Result<()>
where
    T: FromStr + Display,
    <T as FromStr>::Err: Error + 'static + Send + Sync,
{
    let intermediate: T = submission.code.parse()?;
    let source = std::mem::replace(&mut submission.code, intermediate.to_string());
    submission.source_code = Some(source);
    Ok(())
}
